package com.company;

import org.junit.*;
import java.io.*;
//import java.io.FileReader;
//import java.io.IOException;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
public class testCase {
    Main mainFunction;
    String inputNumber;

    /**
     * This code runs before every test 
     */
    @Before
    public void beforeEachTest(){
        mainFunction = new Main();
    }

    /**
     * this code runs after every test
     */
    @After
    public void afterEachTest(){
        System.out.println("congratulations,your testCases passed");
    }
    
    

    @Test
    public void dummyMock(){
        mainFunction = mock(Main.class);
        when(mainFunction.isSparse("12")).thenReturn(true);

        boolean t = mainFunction.isSparse("12");
        verify(mainFunction,atLeastOnce()).isSparse("12");

    }

    /**
     * check wheather input gives wrong format or not 
     * if input given wrong format than this will give an error
     */
    @Test(expected = NumberFormatException.class)
    public void testCase1(){

        assertFalse("first value",mainFunction.isSparse("A12"));
    }
    
    

    /**
     * read input from textFile1
     * check every number is sparse or not
     * @throws IOException
     */
    @Test
    public void testCase2() throws IOException {
//        Main n = new Main();
        BufferedReader vr = new BufferedReader(new FileReader("TextFile1.txt"));
        while ((inputNumber = vr.readLine()) != null) {
            assertFalse("values1", mainFunction.isSparse(inputNumber));
        }
    }
    
    
    
    @Test
    public void testCase3() throws IOException {
//        String s;
        BufferedReader vr = new BufferedReader(new FileReader("TestFile2.txt"));
        while ((inputNumber = vr.readLine()) != null) {
            assertTrue("values2", mainFunction.isSparse(inputNumber));
        }
    }


    /**
     * match our output and user's output with help of assertequals
     * @throws IOException
     */
    @Test
    public void testCase4() throws IOException{
//        String s;
        BufferedReader File1Reader = new BufferedReader(new FileReader("TextFile1.txt"));
        while ((inputNumber = File1Reader.readLine()) != null) {
            int output = Integer.parseInt(inputNumber);
            output = output & (output / 2);
            Assert.assertEquals(output == 0, mainFunction.isSparse(inputNumber));
        }}
}